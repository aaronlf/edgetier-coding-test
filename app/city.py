import requests
import re
import numpy as np
import pandas as pd
from data.score_guidelines import score_weights, weather_scores


class City:
	"""
	Class for finding information on a city
	"""
	def __init__(self,city_name):
		"""
		Upon initialization:
		- The city name gets formatted
		- The csv data of European cities is fetched and saved as a dataframe attribute
		- The city name is verified to be a valid European city (according to the csv data at least)
		- The European city data in the dataframe gets placed into the city_info attribute, a dict
		- Current weather and temperature data are fetched via an API and placed into the city_info
		  attribute.
		:param city_name: This is the name of the city, not yet processed for formatting
		"""
		self.name = self._format_name(city_name)
		self.score_weights = score_weights
		self.scores = []
		self._fetch_csv_data()
		
		if not self.verify_city_exists():
			self.valid = False
			return None
		self.valid = True	
		
		self._place_csv_data_in_city_info()
		self._fetch_weather_and_temperature()
	
	def verify_city_exists(self):
		if self.name in self.all_csv_data['city'].values:
			return True
		return False
	
	def calc_overall_score(self):
		"""
		Calculates the score of a city based on a weighted sum of the city's constituent scores.
		:return: Score out of 10, rounded to 1 decimal place
		"""
		score_sum = 0
		weight_sum = 0
		for metric in score_weights:
			score = getattr(self,f'score_{metric}')()
			if score == None:
				continue
			weight = score_weights[metric]
			score_sum += score*weight
			weight_sum += weight
		overall_score = round(score_sum / weight_sum, 1)
		self.city_info['city_score'] = overall_score
		return overall_score
		
	def score_public_transport(self):
		return self.city_info['public_transport']
		
	def score_temperature(self):
		"""
		Provides a score of the current temperature.
		Uses a Gaussian centred around 23 degrees Celsius and with SD of 6 degrees Celsius, with 
		a peak amplitude of 10.
		:return: Returns the rounded temperature score
		"""
		if self.city_info['current_temperature'] == 'Information not available':
			return None
		gaussian = lambda x,mu,sigma: 10 * np.exp(-0.5 * ((x-mu)/sigma)**2)
		return round(gaussian(self.city_info['current_temperature'],mu=23,sigma=6))
		
	def score_weather(self):
		if self.city_info['current_weather_description'] == 'Information not available':
			return None
		return weather_scores[self.city_info['current_weather_description']]
	
	def _score_quantity_linear(self,input_val,min_val,max_val):
		range_over_all_vals = max_val - min_val
		return round(10 * (input_val - min_val) / range_over_all_vals)
		
	def score_bars(self):
		all_bars = self.all_csv_data['bars'].values
		min_bars, max_bars = min(all_bars), max(all_bars)
		return self._score_quantity_linear(self.city_info['bars'],min_bars,max_bars)
	
	def score_museums(self):
		all_museums = self.all_csv_data['museums'].values
		min_museums, max_museums = min(all_museums), max(all_museums)
		return self._score_quantity_linear(self.city_info['museums'],min_museums,max_museums)
		
	def score_average_hotel_cost(self):
		all_hotel_costs = self.all_csv_data['average_hotel_cost'].values
		min_hotel_cost, max_hotel_cost = min(all_hotel_costs), max(all_hotel_costs)
		return 11 - self._score_quantity_linear(self.city_info['average_hotel_cost'],min_hotel_cost,max_hotel_cost)
	
	def score_crime_rate(self):
		return 11 - self.city_info['crime_rate']
	
	def _format_name(self,name):
		"""
		Removes non-alphanbetic characters from the name
		:param name: Input string, this will be the name of the city.
		:return: Returns formatted string of name.
		"""
		reg = re.compile('[^a-zA-Z]')
		return reg.sub('',name).strip().lower()
				
	def _fetch_csv_data(self):
		"""
		Data is fetched from the csv file and saved to the all_csv_data attribute as a pandas 
		dataframe.
		:return:
		"""
		self.all_csv_data = pd.read_csv('data/european_cities.csv')
		self.all_csv_data['city'] = self.all_csv_data['city'].apply(self._format_name)
		
	def _place_csv_data_in_city_info(self):
		"""
		From the already-loaded csv data, the city_info dictionary gets filled with the relevant 
		values.
		:return:
		"""
		city_csv_data = self.all_csv_data.loc[self.all_csv_data['city'] == self.name]
		self.city_info = city_csv_data.to_dict(orient='records')[0]
		del self.city_info['index'], self.city_info['city']
		self.city_info['city_name'] = self.name
		
	def _fetch_weather_and_temperature(self):
		"""
		Fetches the current weather and temperature of the city and saves it to the city_info
		attribute (dict).
		If no weather information is available from the metaweather API, then both the weather and
		temperature are set to 'Information not available'.
		:return:
		"""
		search_url = f'https://www.metaweather.com/api/location/search/?query={self.name}'
		search_data = requests.get(search_url).json()
		if not search_data:
			weather = temperature = 'Information not available'
		else:
			woeid = search_data[0]['woeid'] # where on earth id
			weather_url = f'https://www.metaweather.com/api/location/{woeid}'
			weather_data = requests.get(weather_url).json()['consolidated_weather'][0]
			weather, temperature = weather_data['weather_state_name'], round(weather_data['the_temp'],2)
		self.city_info['current_weather_description'] = weather
		self.city_info['current_temperature'] = temperature
		
