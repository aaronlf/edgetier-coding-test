from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.city import City


class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self,city_name):
        """
        Retrieve information about a city.
        :param city_name: The city name passed in the URL
        :return:
        """
        city_name = city_name.strip().lower()
        city_obj = City(city_name)
        
        if not self._check_city_name(city_obj):
            return None
        city_obj.calc_overall_score()    
        response = city_obj.city_info

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
        
    def _check_city_name(self,city_obj):
        """
        Check the name is valid
        :param city_obj: City object for a given city name
        :return: Returns None if city name is invalid, else the City object is returned
        """
        if not city_obj.valid:
            response = {'error': 'Invalid city name'}
            self.set_status(HTTPStatus.BAD_REQUEST)
            self.write(json.dumps(response))
            return None
 
        return city_obj
        
