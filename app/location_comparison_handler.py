from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.city import City
from app.location_data_handler import LocationHandler

class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self,city_names_as_string):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for 
        each city.
        :param city_names_as_string: String of city names passed in the URL, separated by commas
        :return:
        """
        city_names = [city.strip().lower() for city in city_names_as_string.split(',')]
        city_names = list(set(city_names)) # to remove possible duplicates
        city_objects = [City(name) for name in city_names]
        
        for city_obj in city_objects:
            if not self._check_city_name(city_obj):
                return None
                
        ranked_city_data = self._rank_cities(city_objects)
        response = {'city_data': ranked_city_data}
        
        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
        
    def _check_city_name(self,city_obj):
		# Couldn't find simple way to achieve DRY via modularization so this is repeated here
        """
        Check the name is valid
        :param city_obj: City object for a given city name
        :return: Returns None if city name is invalid, else the City object is returned
        """
        if not city_obj.valid:
            response = {'error': 'Invalid city name'}
            self.set_status(HTTPStatus.BAD_REQUEST)
            self.write(json.dumps(response))
            return None
 
        return city_obj
	
    def _rank_cities(self,city_objects):
        """
        Gets the score of each city and ranks them in order.
        :param city_names: List of city name strings
        :return: Returns a list of city data dicts, ranked and in ranked order
        """
        city_data = []
        
        for city_obj in city_objects:
            score = city_obj.calc_overall_score() 
            city_data.append({'city_name':city_obj.name,'city_score':score})
			
        ranked_city_data = sorted(city_data,key=lambda c: c['city_score'],reverse=True)
        for i, city in enumerate(ranked_city_data):
            city['city_rank'] = i + 1
           
        return ranked_city_data
