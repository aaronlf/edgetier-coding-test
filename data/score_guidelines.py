
score_weights = {
				'bars':1,
				'museums':1,
				'public_transport':1,
				'crime_rate':2,
				'average_hotel_cost':1,
				'temperature':1.5,
				'weather':1.5
				}
			
weather_scores = {
				'Snow':2,
				'Sleet':3,
				'Hail':2,
				'Thunderstorm':1,
				'Heavy Rain':2,
				'Light Rain':4,
				'Showers':5,
				'Heavy Cloud':6,
				'Light Cloud':8,
				'Clear':10,
				}
